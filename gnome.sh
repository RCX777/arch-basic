#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

# sudo reflector -c Germany -a 12 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Sy

sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload


sudo pacman -S --noconfirm xorg gdm gnome-shell nautilus firefox gnome-keyring gnome-control-center gnome-tweaks gnome-system-monitor

# Some apps
paru -S --noconfirm gnome-terminal-transparency firefox timeshift visual-studio-code-bin teams octave discord spotify

# Fixing teams
# sudo sed -n '1,+9p' /usr/bin/teams > /usr/bin/teams
# sudo echo "nohup \"\$TEAMS_PATH\" \"\$@\" --disable-seccomp-filter-sandbox --disable-namespace-sandbox --disable-setuid-sandbox > \"\$TEAMS_LOGS/teams-startup.log\" 2>&1 &" >> /usr/bin/teams

sudo systemctl enable gdm
/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
sudo reboot
