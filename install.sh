#!/bin/bash

ln -sf /usr/share/zoneinfo/Europe/Bucharest /etc/localtime
hwclock --systohc
sed -i '178s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts
echo root:password | chpasswd

# You can add xorg to the installation packages, I usually add it at the DE or WM install script
# You can remove the tlp package if you are installing on a desktop or vm

pacman -S networkmanager network-manager-applet dialog wpa_supplicant base-devel linux-headers avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils bluez bluez-utils alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack openssh rsync acpi acpi_call edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat iptables-nft ipset firewalld sof-firmware nss-mdns acpid ntfs-3g # reflector

# pacman -S --noconfirm xf86-video-amdgpu
pacman -S --noconfirm nvidia nvidia-utils nvidia-settings nvidia-prime

# grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB #change the directory to /boot/efi is you mounted the EFI partition at /boot/efi

# grub-mkconfig -o /boot/grub/grub.cfg

pacman -S refind  #  refind bootloader
refind-install

# Enabling the zram module (systemd, 4096M)
echo "zram" > /etc/modules-load.d/zram.conf
echo "options zram num_devices=1" > /etc/modprobe.d/zram.conf
echo "KERNEL==\"zram0\", ATTR{disksize}=\"4096M\",TAG+=\"systemd\"" > /etc/udev/rules.d/99-zram.rules
printf "[Unit]\nDescription=Swap with zram\nAfter=multi-user.target\n\n[Service]\nType=oneshot\nRemainAfterExit=true\nExecStartPre=/sbin/mkswap /dev/zram0\nExecStart=/sbin/swapon /dev/zram0\nExecStop=/sbin/swapoff /dev/zram0\n\n[Install]\nWantedBy=multi-user.target\n" > /etc/systemd/system/zram.service
systemctl enable zram

# Fixing internal audio for asus rog laptops
printf "options snd_hda_intel model=asus-zenbook" > /etc/modprobe.d/alsa-base.conf

# Adding parallel compilation to makepkg
echo "MAKEFLAGS=\"-j\$(nproc)\"" >> /etc/makepkg.conf

# Installing the 'paru' AUR helper
git clone https://aur.archlinux.org/paru.git
cd paru-git
makepkg -si
cd ..
rm -rf ./paru-git


systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable sshd
systemctl enable avahi-daemon
# systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable firewalld
systemctl enable acpid

useradd -m rcx
echo rcx:password | chpasswd
usermod -aG wheel,storage,audio,video,optical,sudo rcx

echo "rcx ALL=(ALL) ALL" >> /etc/sudoers.d/rcx


printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"




